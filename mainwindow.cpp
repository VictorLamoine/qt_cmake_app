#include "mainwindow.hpp"

MainWindow::MainWindow(QWidget *parent)
  : QMainWindow(parent)
{
  QWidget *central_widget = new QWidget(this);
  QVBoxLayout *layout = new QVBoxLayout;
  central_widget->setLayout(layout);
  setCentralWidget((central_widget));
  layout->addWidget(new QLabel("Hello world"));
}
